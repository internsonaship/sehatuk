/**
 * @class up
 * @returns
 * Main program
 * Updates the medicines database with the new ones posted on the anam website: www.anam.ma
 */
var request = require('request');
require('newrelic');
var Bottleneck = require("bottleneck");
var assert = require('chai').assert;
var nodemailer = require('nodemailer');
const pug = require('pug');
var cheerio = require('cheerio'),
    cheerioTableparser = require('cheerio-tableparser');
var db_name='medication';
var nano= require('nano')('http://localhost:5984');
var db = nano.db.use(db_name), per_page = 100
  , params   = {include_docs: true, descending: true};
var err_db= nano.db.use('errors');
var changes_db= nano.db.use('changes');
var options = {
  method: 'GET',
  headers: {'user-agent': 'node.js'}
};
var err_list=[];


var couchdb='http://localhost:5984/_utils/document.html?';

//----------------------------------------------------------------------------------//
//--------------------------------------Admin---------------------------------------//
//----------------------------------------------------------------------------------//
/**
 * Requires the module admin-npm.
 * Checks the environment state of the machine.
 * @method admin
 * @returns an API to check the state on browser 
 * 
 * 
 */
const admin = require('admin');
 
admin.configure({
  http: { // optional 
    bindAddress: '127.0.0.1', // default 
    port: 2999 // default 
  },
 
  plugins: [
    require('admin-plugin-index')(),
    //require('admin-plugin-report')(),
    require('admin-plugin-environment')(),
    //require('admin-plugin-profile')(),
    require('admin-plugin-terminate')(),
    require('admin-plugin-config')({
      config: {
        // An application config goes here. This config object will be 
        // visible in the admin UI and via the admin REST endpoints. 
        secret: '42',
        port: 8080
      }
    }),
    require('admin-plugin-healthcheck')({
      checks: {
        // Define multiple healthchecks which check critical components 
        // in the system. The following example shows valid return values. 
        random() {
          const v = Math.random();
          if (v > 0.8) {
            throw new Error('Random value >0.8');
          } else if (v > 0.3) {
            return "Healthy like an application that isn't used.";
          } else {
            return Promise.reject('Something bad happened here…');
          }
        }
      }
    })
  ]
});
 
admin.start();
//----------------------------------------------------------------------------------//
//--------------------------------------Names---------------------------------------//
//----------------------------------------------------------------------------------//

var base= "http://www.anam.ma/wp-admin/admin-ajax.php?action=get_Medicament&term=";
/**
 * Gives a letter by using ascii code.
 *
 * @method get_letter
 * @param cnt {number}
 */
function get_letter (cnt) {
    return(String.fromCharCode(97+cnt));}
  
/**
 * Gives a url by adding a letter to a base incomplete url.
 *
 * @method get_url
 * @param base {String}
 * @param letter {char}
 * @param callback {function}
 */

function get_url(base,letter,callback){
  var url=base+letter;
  callback(url);}


/**
 * Sends request to fetch the content of the url. 
 * It formats the content to take the names of all the medicines listed there.
 *
 * @method send_names_req
 * @param url {String}
 * @param retry_conn_cnt {number}
 * @param error_list {array}
 * @param callback {function}
 */
function send_names_req(url,retry_conn_cnt,error_list,callback){
  console.log(url);
  request(url, options,function (error, response, html) {
  
      if (!error)  {
        
        check_response_status(response.statusCode,url,error_list,function(err,error_list){
          if(err){

              callback(err,null,error_list);}
          else{
          check_response_body(response.body,url,error_list,function(err,error_list){
            if(err){
              
              callback(err,null,error_list);
            }
            else{
              try {
                   names = JSON.parse(response.body);
                } catch(e) {
                  err_obj('SyntaxError: Unexpected token while parsing url: '+url,function(obj){
                    error_list.push(obj);
                    callback('Error while parsing JSON','not able to parse '+url,error_list); 
                  });
                  
                }
              callback(null,names,error_list); 
            }
          });  
          }
        });
        }   
      else{
        connection_errors(error.code,url,error_list,function(err){
          
          if(!err){
            retry_names_url(retry_cnt,function(retry,retry_conn_cnt){
                  if(retry){
                    send_names_req(url,retry_cnt,error_list,callback);
                    }
                  else
                    callback(true,null,error_list); });
          }
          else { if(err!='no internet'){
              url_errors(error,url,error_list, function(err,error_list){
                callback(err.code,null,error_list);
                                });}
              else{ callback(err,null,error_list);}}
                
        });
        }
    });
}
//----------------------------------------------------------------------------------//
//---------------------------------------Errors-------------------------------------//
//----------------------------------------------------------------------------------//

/**
 * Sends an error of invalid url and pushes it into the error list. 
 *
 * @method invalid_url
 * @param url {String}
 * @param error_list {array}
 * @param callback {function}
 */
function invalid_url(url,error_list,callback){
  err_obj("invalid url: "+url,function(obj){
    error_list.push(obj);
    callback("Invalid url: "+url,error_list);
                  });
  
}

/**
 * Sends an error of unauthorized access and pushes it into the error list. 
 *
 * @method unauthorized_access
 * @param url {String}
 * @param error_list {array}
 * @param callback {function}
 */
function unauthorized_access(url,error_list, callback){
  err_obj("unauthorized access "+url,function(obj){
    error_list.push(obj);
    callback("unauthorized access "+url,error_list);
                  });
}

/**
 * Checks the response status to see if there is an error. 
 *
 * @method check_response_status
 * @param response_status {number}
 * @param url {String}
 * @param error_list {array}
 * @param callback {function}
 */
function check_response_status(response_status,url,error_list,callback){

    if(response_status==404){
         invalid_url(url,error_list,callback);
            }
    else if(response_status==401){
         unauthorized_access(url,error_list, callback);
            }
    else if(response_status==200){
      callback(null,error_list);
    }

}

/**
 * Checks the response body to see if the body has content. 
 *
 * @method check_response_body
 * @param body {String}
 * @param url {String}
 * @param error_list {array}
 * @param callback {function}
 */
function check_response_body(body,url,error_list,callback){
 // console.log(body[0]);
  if(body=='[]'){
    //console.log("This URL: "+url+" has no content.");
    err_obj("This URL: "+url+" has no content.",function(obj){
      error_list.push(obj);
      callback("empty",error_list);
                  });
    }
  else
    callback(null,error_list);
}

/**
 * Checks for url errors: invalid url or unauthorized access. 
 *
 * @method url_errors
 * @param error {String}
 * @param url {String}
 * @param error_list {array}
 * @param callback {function}
 */
function url_errors(error,url,error_list, callback){
  
  if(error=='Error: Invalid URI "'+url+'"'){
    invalid_url(url,error_list, function(err,error_list){
         callback(err,retry_cnt,error_list);
     
    });
  }
  else if(error.statusCode==401){
    unauthorized_access(url,error_list, function(err,error_list){
      callback(err,retry_cnt,error_list);
    });
  }
  else callback(error,error_list);
}


/**
 * If the error code it receives is ENOTFOUND, i.e. an internet connection error it returns true. 
 *
 * @method no_conn
 * @param error {String}
 * @returns Default: False. True if error {boolean}
 */

function no_conn(error){
      if(error=='ENOTFOUND'){
        return true;
      }
      else
        return false;
}


/**
 * Check the existence of internet connection by pinging (sending request to) www.google.com. 
 *
 * @method connection_err
 * @param callback {function}
 */
function connection_err(callback){
 var halted;
   request('http://www.google.com', function (error, response,html){
      
      if(error){
              halted=no_conn(error.code);
              callback(error.code,halted);
              }
      
      else {
        halted= false;
         callback(null,halted); 
        }
  } );    
  
}

/**
 * Keeps looking for internet connection for a certain number of times. 
 *
 * @method keep_trying
 * @param error {String}
 * @param counter {number}
 * @param callback {function}
 */
function keep_trying(error,counter,callback){
  
  if(counter==0){
    callback("no internet",true);

  }
  else if(counter>0 ){
    
    connection_err(function(err,halted){
      if(halted){
        counter--;
     setTimeout(function(){ keep_trying(err,counter,callback);},10000);
       
      }
      else {

        callback(null,halted);}
    });
  }
}

/**
 * Checks if there is a connection error (internet issue or timeout or server down). 
 *
 * @method connection_errors
 * @param error {String}
 * @param url {String}
 * @param error_list {array}
 * @param callback {function}
 */
function connection_errors(error,url,error_list,callback){
    if(error=='ENOTFOUND'){
      err_obj("connection error",function(obj){
        error_list.push(obj);
        connection_err(function(err,halted){
           if(!halted){
                  if(err)
                    err_obj(err,function(obj){
                       error_list.push(obj);});
                  callback(err,error_list);
                      }
            else{
                      keep_trying(err,10,function(err,halted){
                      //  console.log(err);
                        callback(err,error_list);
                      });
                    }
          });
                  });
      
    }
    else if(error=='ECONNECTION'){
      timeOut_err(error_list,function(err,error_list){
          callback(err,error_list);
            });
  }
  else callback(error,error_list);

}

/**
 * Sends an error of timeout of url and pushes it into the error list. 
 *
 * @method timeOut_err
 * @param error_list {array}
 * @param callback {function}
 */
function timeOut_err(error_list,callback){
  
    console.log("Timeout issue, we will retry in a while...");
    err_obj('Timeout issue',function(obj){
      error_list.push(obj);
      setTimeout(function(){callback(null,error_list);},3000);
                  });
      
}

/**
 * Sends an error (when data has less or more properties than the ones ususally used) and pushes it into the error list. 
 *
 * @method format_error
 * @param data {array}
 * @param error_list {array}
 * @param callback {function}
 */
function format_error(data,error_list,callback){
  
  if(data.length<11 || data.length>11){
    err_obj("format error: "+data,function(obj){
      error_list.push(obj);
      callback(true,error_list);
                  });
  }
  else
  callback(null,error_list);
}

/**
 * Sends an error (when data has a different format than the usual one i.e. when data doesn't have the usual standard propreties) and pushes it into the error list. 
 *
 * @method format_error2
 * @param data {array}
 * @param error_list {array}
 * @param callback {function}
 */
function format_error2(data,error_list,callback){
  
  if((data[0][0]!='Code EAN-13')||(data[1][0]!='Médicament')||(data[2][0]!='Substance active (DCI) & dosage')
    ||(data[3][0]!='Forme & Présentation')
    ||(data[4][0]!='Prix Public de Vente (*PPV)')||(data[5][0]!='Prix base remboursement (PPV)')||(data[6][0]!='Prix Hospitalier (**PH )')
    ||(data[7][0]!='Prix base remboursement (PH)')
    ||(data[8][0]!='Classe Thérapeutique')
    ||(data[9][0]!='P:Princeps G:Generique')||(data[10][0]!= 'Remboursement')){
    err_obj("format error: "+data,function(obj){
      error_list.push(obj);
      callback(true,error_list);
                  });

  }
  else callback(null,error_list);
}

//----------------------------------------------------------------------------------//
//---------------------------------------Retry--------------------------------------//
//----------------------------------------------------------------------------------//

/**
 * Check if the retry counter hasn't reached zero yet.  
 *
 * @method retry_names_url
 * @param retry {number}
 * @param callback {function}
 */
function retry_names_url(retry,callback){
  retry--;
  if(retry==0){
    callback(false,retry);
  }
  else
    callback(true,retry);
}

/**
 * While retries counter hasn't reached 0, a request with url is being sent.
 *
 * @method retry_url
 * @param url {string}
 * @param retry {number}
 * @param callback {function}
 */
function retry_url(url,retry,callback){
  retry--;
  if(retry==0){
    callback(false,retry);
  }
  else if(retry>0){
    request(url,options,function(error,response,html){
      callback(true,retry,error,response,html);
    });}
}

/**
 * While retry counter is different than 0, the element is inserted. 
 *
 * @method retry_insert
 * @param info {object}
 * @param retry {number}
 * @param changes {array}
 * @param callback {function}
 */
function retry_insert(info,retry,changes,callback){
  retry--;
  if(retry==0){
     callback(false,changes);
  }
  else if (retry>0){
    insert_db(info,changes,function(err,info,db_err,changes){
      if(err)
        retry_insert(info,retry,changes,callback);
      else
        callback(true,changes);
    });}
}


//----------------------------------------------------------------------------------//
//--------------------------------------Database------------------------------------//
//----------------------------------------------------------------------------------//

/**
 * creates an id for each element. 
 *
 * @function get_id
 * @param info {object}
 * @returns id
 */
function get_id(info){
  if(info['Code']==""){
    id=info.Medicament+" " +info['Forme & Presentation']+" "+info['Substance active (DCI) & dosage'];
    id=id.trim();
  }
  else{
      id=info['Code'];}
  return id;

}

/**
 * Inserts element in the database.
 *
 * @method insert_db
 * @param info {object}
 * @param changes {array}
 * @param callback {function}
 */
function insert_db(info,changes,callback){
  var id=get_id(info);
  db.insert(info, id, function(err, body){
   if(!err){
      change_obj(info,function(obj){
                    changes.push(obj);
                    callback(null,info,null,changes);
                   });
      
  }
    else{
        if(err.message === 'no_db_file' ) {
          var db_err="No database with that name exists. We will try to create one";
             nano.db.create(db_name, function () {
              retry_insert(info,2,changes,function(added,changes){
                  if(!added){
                    callback('not added', info, db_err, changes);
                   }
                  else{
                    callback('added' ,info, db_err, changes);}
              });
            });
          }
        else {
          callback(err.error,info,null,changes);}}
});
}


//----------------------------------------------------------------------------------//
//---------------------------------------Parser-------------------------------------//
//----------------------------------------------------------------------------------//

/**
 * For each name parameter given to it, it encodes it and returns the url to access to its data.
 *
 * @method get_names_url
 * @param name {String}
 * @param callback {function}
 */

function get_names_url(name,callback){
    callback(null,'http://www.anam.ma/regulation/guide-medicaments/recherche-de-medicaments-par-nom/?tags='+encodeURIComponent(name.trim())+'&search=type1/');
}

/**
 * Using cheerio module, the medicines table is extracted from the html passed to the function.
 *
 * @method get_data
 * @param html {html}
 * @param callback {function}
 */
function  get_data(html,callback){
    var $ = cheerio.load(html);
    cheerioTableparser($);
    var data=$('.list-medicament').parsetable(false, false, true);
    callback(data);
}

/**
 * This function pre-tests data for format error (format_error) in order to catch it now before moving data to further stages.
 *
 * @method format_data_parsing
 * @param data {object}
 * @param error_list {array}
 * @param callback {function}
 */



function format_data_parsing(data,error_list, callback){
    format_error(data,error_list,function(err,error_list){
       callback(err,data,error_list);
      
    });   
}

/**
 * This function tests data for format error (format_error2), if none it formats 
 * the data given to it to a JSON object following a standard format.
 *
 * @method info_object
 * @param data {object}
 * @param index {number}
 * @param error_list {array}
 * @param callback {function}
 */
function info_object(data,j,error_list,callback){
  format_error2(data,error_list,function(err,error_list){
    if(!err){
      var info= {
        'Code': data[0][j],
        'Medicament': data[1][j],
        'Substance active (DCI) & dosage': data[2][j] ,
        'Forme & Presentation': data[3][j],
        'Prix Public de Vente (*PPV)': data[4][j],
        'Prix base remboursement (PPV)': data[5][j],
        'Prix Hospitalier (**PH )': data[6][j],
        'Prix base remboursement (PH)': data[7][j],
        'Classe Therapeutique': data[8][j] ,
        'P:Princeps G:Generique': data[9][j],
        'Remboursement': data[10][j]
            };
            callback(err,info,error_list);
    }
    else callback(err,null,error_list);
  });   
}

/**
 * This function calls fetches data and formats it then inserts it in the database while keeping track of changes and errors.
 * This function performs by calling other functions such as get_data() ,  format_data_parsing() , info_object() , check_exist()...etc.
 *
 * @method process_data

 * @param array {array}
 * @param html {html}
 * @param error_list {array}
 * @param retry_counter {number}
 * @param changes {array}
 * @param callback {function}
 
 */
function process_data(arr,html,error_list,retry_info,changes,callback){
  
  var m=0;

  get_data(html,function(data){

    if(data.length!=0){
     format_data_parsing(data,error_list, function(err,data,error_list){
        if(!err){
          if(data[1].length>1){
            for(var j=1;j<data[1].length;j++)
            { console.log(j);
              info_object(data,j,error_list,function(err,info,error_list){
                if(!err){
                  console.log('no err in info_object');
                  arr.push(info); 
                  check_exist(info,error_list,changes,function(err,info,error_list,changes){
                    m++;
                    console.log('process data m: '+m);
                    console.log('data length: '+(data[1].length-1));
                    if(err=='not added'){
                      err_obj(info.Code+" "+err,function(obj){
                        error_list.push(obj);
                        if(m==data[1].length-1){ 
                          console.log('exit');
                          return callback(err,arr,error_list,changes,data[1].length);} 
                        });}
                            
                        else if(err=='conflict'){ 
                          retry_insert(info,retry_info,changes,function(added,changes){
                            if(!added)
                              err_obj(get_id(info)+ ' not added to database.',function(obj){
                                error_list.push(obj); 
                              });
                            
                            if(m==data[1].length-1){ 
                              console.log('exit');
                              return callback(err,arr,error_list,changes,data[1].length);} 
                            });}
                        else{ 
                          console.log('other type of err or maybe no err');
                          if(m==data[1].length-1){ 
                            console.log('exit');
                            return callback(err,arr,error_list,changes,data[1].length);} }
                          });}
                else{
                  console.log('error in info_object');
                  if(m==data[1].length-1){ 
                    return callback(err,arr,error_list,changes,data[1].length);} }
                  }); }
               }
        else {console.log('stuck: data[1].length<=1');
          }}
      else {
        console.log('there is a format err');
         return callback(err,arr,error_list,changes,undefined);}
    });}
    else{
       return  callback('undefined',arr,error_list,changes,undefined);
    }
});}


/**
 * This function checks if there is a second page of a medicine, if it exists it sends a request to fetch its content.
 *
 * @method check_second_page
 *
 * @param url {string} url of the first page of the medicines.
 * @param error_list {array} List of errors.
 * @param array {array} List of data (medicines) retrieved so far.
 * @param changes {array} List of changes.
 * @param return callback {function}
 *
 */
function check_second_page(url,error_list,arr,changes,callback){
  url= url.replace('http://www.anam.ma/regulation/guide-medicaments/recherche-de-medicaments-par-nom/','http://www.anam.ma/regulation/guide-medicaments/recherche-de-medicaments-par-nom/page/2/');
  request(url,options,function(err,response,html){
    send_req(err,response,html,url,10,2,error_list,arr,changes,function(err,elems,error_list,changes){
      return callback(elems,error_list,changes);
                    });
});}

/**
 * This function gets the response, and the html retrieved from the request and sends the html to be processed by the function process_data. If the size of data reached the max of page display, the second page is checked in case it has content.
 * This functio also handes errors (connection and url errors).
 * @method send_req
 *
 * @param error {object} error that happened during request.
 * @param response {object} response of the request.
 * @param html {html} the body of the response.
 * @param url {string} the url that is being processed.
 * @param retry_url_counter {number} number of retries allowed for the url.
 * @param retry_insert_counter {number} number of retries allowed for the data to get inserted.
 * @param error_list {array} List of errors.
 * @param array {array} List of data (medicines) retrieved so far.
 * @param changes {array} List of changes.
 * @param callback {function}
 *
 */
function send_req(error,response,html,url,retry_url_cnt,retry_info,error_list,arr,changes,callback){
 
      if (!error)  {
        check_response_status(response.statusCode,url,error_list,function(err,error_list){
          if(err){
              return callback(err,arr,error_list,changes);}
          else{
            check_response_body(response.body,url,error_list,function(err,error_list){
              if(err){  
                callback(err,arr,error_list,changes);
            }
              else{
                process_data(arr,html,error_list,retry_info,changes,function(err,new_arr,error_list,changes,data_length){
                  if(!err){
                    if(data_length>=10){
                      //console.log("checking 2nd page");
                      check_second_page(url,error_list,arr,changes,function(elems,error_list,changes){
                      return callback(err,elems,error_list,changes);});}
                    else{
                     // console.log("no second page");
                      return callback(err,arr,error_list,changes);}}
                  else if(err=='undefined'){
                   // console.log('printing this '+err);
                    err_obj('No data retrieved from '+url,function(obj){
                                error_list.push(obj);
                                 return callback(err,new_arr,error_list,changes); 
                             });
                    }
                  else return callback(err,new_arr,error_list,changes);
               }); } });  }
        });}   
      else{
        connection_errors(error.code,url,error_list,function(err,error_list){
          
          if(!err){
            retry_url(url,retry_url_cnt,function(retry,retry_url_cnt,err,res,html){
              if(retry){
                send_req(err,res,html,url,retry_url_cnt,retry_info,error_list,arr,changes,callback);
              }
              else{
                 err_obj('Not able to access: '+url+'\n The server might be down ',function(obj){
                                error_list.push(obj);
                                  return callback(true,arr,error_list,changes);
                             });
              } });
          }
          else {
            if(err!='no internet'){
              url_errors(error,url,error_list, function(err,error_list){
                return callback(err.code,arr,error_list,changes);
                                });}
              else{ return callback(err,arr,error_list,changes);}}
        });
        }
}
//----------------------------------------------------------------------------------//
//---------------------------------------Update-------------------------------------//
//----------------------------------------------------------------------------------//

/**
 * This function checks if an element that we read from the website, exists already in our database. if ir does then we check for any updates and perform them, if it doesn't we add (insert) it.
 * 
 * @method check_exist
 *
 * @param info {object} the element we read from the website (medicine).
 * @param error_list {array} List of errors.
 * @param changes {array} List of changes.
 * @param callback {function}
 *
 */
function check_exist(info,error_list,changes,callback){
  var db_url=couchdb+db_name+'/'+encodeURIComponent(get_id(info));
  db.get(get_id(info),function(err,doc){
    if(err){
       if(err.statusCode==404){
          console.log(err.statusCode);
         console.log("not found");
         insert_db(info,changes,function(err,info,db_err,changes){
           if(db_err!=null)
             err_obj(db_err,function(obj){
                error_list.push(obj);
                             });
            callback(err,info,error_list,changes);
         });
       }}
    else{
      console.log('found');
          if((doc['Code']!=info['Code'])
            ||(doc['Medicament']!=info['Medicament'])
            ||(doc['Prix base remboursement (PH)']!==info['Prix base remboursement (PH)'])
            ||(doc['Prix Public de Vente (*PPV)']!==info['Prix Public de Vente (*PPV)'])
            ||(doc['Prix base remboursement (PPV)']!==info['Prix base remboursement (PPV)'])
            ||(doc['Prix Hospitalier (**PH )']!==info['Prix Hospitalier (**PH )'])
            ||(doc['Classe Therapeutique']!==info['Classe Therapeutique'])
            ||(doc['P:Princeps G:Generique']!==info['P:Princeps G:Generique'])
            ||(doc['Remboursement']!==info['Remboursement']))
           {
            update(doc,info,error_list,changes,function(err,info,error_list,changes){
            return callback(err,info,error_list,changes);
          });}
          else {
           return callback(err,info,error_list,changes);}
      
      
    }
  });
}

/**
 * Creates a main key, for each element, that can be used to search the database.
 *
 * @method get_key
 * @param elem {object} a medicine.
 * @returns key {string}
 */
function get_key(elem){
  var key= String(elem.Medicament+" " +elem['Forme & Presentation']+" "+elem['Substance active (DCI) & dosage']);
  return key;
}


/**
 * Uses the function get_key() to get the key and search the new data gotten earlier from the anam website to find an element. If found flag=1 else flag=0.
 *
 * @method find_elem
 * @param element {object} a medicine from the database.
 * @param data {array} the data retrieved from the website.
 * @param callback {function}
 */
function find_elem(elem,new_data,callback){
  var key=get_key(elem);
  var flag=0;
  for(var i=0;i<new_data.length;i++){
   // console.log('found elem flag='+flag);
    var temp_key=get_key(new_data[i]);
    //console.log(key+'  '+temp_key);
    if(key==temp_key){
      flag=1;
      break;}
  }
  return callback(flag);

}

/**
 * This function updates a medicine in the database
 * 
 * @method update
 *
 * @param old_element {object} old element that exists in  the databse.
 * @param new_element {object} new element that has the modifications.
 * @param error_list {array} List of errors.
 * @param changes {array} List of changes.
 * @param callback {function}
 *
 */
function update(old_elem,new_elem,error_list,changes,callback){
  db.get(get_id(old_elem),function(err,doc){
      if(!err) { old_elem.Code= new_elem.Code;
               old_elem['Medicament']= new_elem['Medicament'];
               old_elem['Substance active (DCI) & dosage']= new_elem['Substance active (DCI) & dosage'];
               old_elem['Forme & Presentation']= new_elem['Forme & Presentation'];
               old_elem['Prix base remboursement (PH)']= new_elem['Prix base remboursement (PH)'];
               old_elem['Prix Public de Vente (*PPV)']= new_elem['Prix Public de Vente (*PPV)'];
               old_elem['Prix base remboursement (PPV)']= new_elem['Prix base remboursement (PPV)'];
               old_elem['Prix Hospitalier (**PH )']= new_elem['Prix Hospitalier (**PH )'];
               old_elem['Classe Therapeutique']= new_elem['Classe Therapeutique'];
               old_elem['P:Princeps G:Generique']= new_elem['P:Princeps G:Generique'];
               old_elem['Remboursement']=new_elem['Remboursement'];
               db.insert(old_elem,function(err,body){
                 if(!err){
                   console.log('changed');

                   change_obj(old_elem,function(obj){
                    console.log(changes);
                    changes.push(obj);
                    return callback(err,old_elem,error_list,changes);
                   });
                   
                  
                 }
                 else{
                  err_obj(err,function(obj){
                    error_list.push(obj);
                    return callback(err,old_elem,error_list,changes);
                             });}
               });}
      else {
         err_obj(err,function(obj){
          error_list.push(obj);
          return callback(err,old_elem,error_list,changes);
                             });}
  });
}


/**
 * This function checks if an element from the database doesn't exist in the new data from the website. If it doesn't exist then we update the element's remboursable to Non-remboursable.
 * @method find_non_remboursable_elems
 *
 * @param element {object} element from the database.
 * @param new_data {object} new data retrieved from the website.
 * @param error_list {array} List of errors.
 * @param retry_info_counter {array} Number of retries allowed to insert an element
 * @param changes {array} List of changes.
 * @param callback {function}
 *
 */
function find_non_remboursable_elems(elem,new_data,error_list,retry_info,changes,callback){

  find_elem(elem,new_data,function(flag){
   
    if(flag==0 && elem['Remboursement']=='Remboursable' ){
      var new_elem={Code: elem.Code,
        'Medicament': elem['Medicament'],
        'Substance active (DCI) & dosage': elem['Substance active (DCI) & dosage'] ,
        'Forme & Presentation': elem['Forme & Presentation'],
        'Prix Public de Vente (*PPV)': elem['Prix Public de Vente (*PPV)'],
        'Prix base remboursement (PPV)': elem['Prix base remboursement (PPV)'],
        'Prix Hospitalier (**PH )': elem['Prix Hospitalier (**PH )'],
        'Prix base remboursement (PH)': elem['Prix base remboursement (PH)'],
        'Classe Therapeutique': elem['Classe Therapeutique'],
        'P:Princeps G:Generique': elem['P:Princeps G:Generique'],
        'Remboursement': 'Non-Remboursable'};
    
      update(elem,new_elem,error_list,changes,function(err,info,error_list,changes){
        var updated=false;

        if(!err)
          updated=true

        return callback(updated,error_list,changes);
      });}
    else return callback(null,error_list,changes);
  });
      
      
      }


/**
 * This function retrieves all docs from the database and calls the function find_non_remboursable_elems() on each document.
 * @method get_docs
 *
 * @param data {array} new data retrieved from website.
 * @param new_data {object} new data retrieved from the website.
 * @param error_list {array} List of errors.
 * @param retry_info_counter {array} Number of retries allowed to insert an element
 * @param changes {array} List of changes.
 * @param callback {function}
 *
 */
function get_docs(data,error_list,retry,changes,callback){
 
  var m=0;
  db.list(params, function(error,body,headers) {
    if(!error){
        for(var i=0;i<body.total_rows;i++){
          
          var doc=body.rows[i].doc;
          //use limiter
          find_non_remboursable_elems(doc,data,error_list,retry,changes,function(err,error_list,changes){
            //check here
            m++;

            if(m==body.total_rows)
            return callback(null,error_list,changes);
          });
          
      }}
    else {
      err_obj(error,function(obj){
          error_list.push(obj);
          return callback(error,error_list,changes);
                             });}
  });

}

//----------------------------------------------------------------------------------//
//----------------------------------------Main--------------------------------------//
//----------------------------------------------------------------------------------//

var limiter = new Bottleneck (1,10);

function submain(letter,queue,m,error_list,changes,callback){
  var m=0;

    get_url(base,letter,function(url){

      send_names_req(url,10,error_list,function(err,names,error_list){
        if(err){
          return callback(queue,error_list,changes);
        }
        else{
          for(var j=0;j<names.length;j++){
            get_names_url(names[j].value,function(err,url){

            limiter.submit(request,url,options,function(error, response, html){

            send_req(error,response,html,url,10,2,error_list,queue,changes,function(err,elems,error_list,changes){
              console.log(url );
              m++;
              console.log('m '+m+letter);
              console.log(names.length);
              if(m==names.length){console.log('done with this letter');
                return  callback(elems,error_list,changes);}
              else console.log('not yet');
                         });  });
                    });}
              }
      });
    });
}





function main(queue,cnt,error_list,changes,callback){
   var m=0;
   check_couchdb(function(err){
      if(!err){
        if(cnt<26){
          var letter=get_letter(cnt);
          console.log(letter);
          submain(letter,queue,m,error_list,changes,function(queue,error_list,changes){
           // q.concat(queue);
            cnt++;
            console.log(cnt);
            main(queue,cnt,error_list,changes,callback);
          });}
        else{
          get_docs(queue,error_list,2,changes,function(error,error_list,changes){
            console.log(queue);
            if(!error){
              return callback(queue,error_list,changes);
            }
          });
        }}});

}


/**
 * Insert elements in the database. Mainly used with the changes and errors.
 *
 * @method insert_elems
 * @param elems {object}
 * @param db {string}
 * @param callback {function}
 */
function insert_elems(elems,db,callback){
  
  db.insert(elems,elems.date, function(err, body){
    
    if(!err){
      console.log('added');
      return callback(null);
     }
    else{
    
        if(err.message === 'no_db_file' ) {
          var db_err="No database with that name exists. We will try to create one";
            // create database and retry
             nano.db.create(db, function () {
              insert_elems(elems,db,callback);
              
            });
          }
        else {
        
          return callback(err.error);}}
});}


/**
 * Make the changes list and the error list into objects. Then, insert the changes in the changes db and the errors in the errors db.
 *
 * @method changes_errors
 * @param errors_list {array}
 * @param changes_list {array}
 * @param callback {function}
 */
function changes_errors(error_list,changes_list,callback){
    var errs={'date':get_date()+' '+get_time(),'Number of errors':error_list.length,'errors':error_list};
    var changes={'date':get_date()+' '+get_time(),'Number of changes':changes_list.length,'changes':changes_list};
    //var errs=JSON.parse(error_list);
    insert_elems(errs,err_db,function(err1){
      insert_elems(changes,changes_db,function(err2){
        if(!err1){
          if(!err2)
            return callback(errs,changes);
          else
            callback(errs,null);
        }
        else{
          if(!err2)
            return callback(null,changes);
          else
            return callback(null,null);
        }

    });
    });
}

/**
 * Check if couchdb is running.
 *
 * @method check_couchdb
 * @param callback {function}
 */
function check_couchdb(callback){
  db.get('id',function(err,doc){
    if(err.code=='ECONNREFUSED'){
          console.log('Please make sure to start couchdb');
          return callback(true);}
    else{
      callback(false);
    }
  });
}


main([],0,[],[],function(q,error_list,changes_list){
        changes_errors(error_list,changes_list,function(errs,changes){
          if(errs!=null && changes!=null){
            send_email(errs,changes,function(result){
              console.log(result);
              return 'done';
            });
      }  
          else {console.log("A document with that id exists") ;
              return 'done';} });
      

    });  
   

//----------------------------------------------------------------------------------//
//---------------------------------------Email--------------------------------------//
//----------------------------------------------------------------------------------//

/**
 * Sets the template that will be sent by email to the users.
 *
 * @method set_template
 * @param errors {object}
 * @param changes {object}
 * @param callback {function}
 */
function set_template(errors,changes,callback){
  const compiledFunction = pug.compileFile('messages.pug');
  
  var f=compiledFunction({
    changes: changes,
    errors: errors,
    date: get_date(),
    errs_number: errors['Number of errors'],
    changes_number: changes['Number of changes'],
    err_link: couchdb+'errors/'+encodeURIComponent(errors.date),
    changes_link: couchdb+'changes/'+encodeURIComponent(changes.date),
  });
  return callback(f);
}

/**
 * Sends email to specific admins informing them with the changes and errors that happened during the update.
 *
 * @method send_email
 * @param errors {object}
 * @param changes {object}
 * @param callback {function}
 */
function send_email(errors,changes,callback){
  set_template(errors,changes,function(content){
 
    var transporter = nodemailer.createTransport({
      host: 'smtp.gmail.com',
      port: 465,
      secure: true, // secure:true for port 465, secure:false for port 587
      auth: {
          user: '****@gmail.com', //insert email of the user here // for many users '**@b.com,***@b.com, etc' 
          pass: '***'
      }
    });

    var mailOptions = {
      from: 's.hamdi@dialy.net',
      to: '***@gmail.com', // for many users '**@b.com,***@b.com, etc' 
      subject: 'Sending Email using Node.js',
      html: content
    };

    transporter.sendMail(mailOptions, function(error, info){
      if (error) {
        return callback(error);
      } else {
        callback('Email sent');
      }
    });
  });
  
}
//----------------------------------------------------------------------------------//
//--------------------------------------Objects-------------------------------------//
//----------------------------------------------------------------------------------//

/**
 * Gives the date formatted like Weekday day month year  
 *
 * @method get_date
 * @returns date Weekday day month year 
 */
function get_date(){
  var d=(new Date()).toString().split(' ');
  return(d[0]+' '+d[2]+' '+d[1]+' '+d[3]);
}

/**
 * Gives the time formatted like hrs:mins:secs  
 *
 * @method get_time
 * @returns time hrs:mins:secs
 */
function get_time(){
  var d = new Date(); 
 return(d.getHours()+':'+d.getMinutes()+':'+d.getSeconds());
}

/**
 * Makes a given error into an object that has the time and the specific error.   
 *
 * @method err_obj
 * @param err {String}
 * @param callback {function}
 */
function err_obj(err,callback){
    var obj={
      time:get_time(),
      err:err
    };
   callback(obj);

}

/**
 * Makes a given change into an object that has the time and the specific change.   
 *
 * @method change_obj
 * @param change {String}
 * @param callback {function}
 */
function change_obj(change,callback){
    var obj={
      time:get_time(),
      change: change
    };
     callback(obj);
  
}
