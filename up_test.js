var rewire = require('rewire');
var assert = require('chai').assert;
var app = rewire('./up.js');




no_conn = app.__get__('no_conn');
describe('no_conn()', function() {
  
  var tests = [
    {args: ['ENOTFOUND'],    expected: true},
    {args: ['ECONNECTION'],    expected: false},
    {args: ['blabla'],    expected: false}

  ];

  tests.forEach(function(test) {
    
    it('checks for connection ' , function() {
      
      var res = no_conn.apply(null, test.args);
      assert.deepEqual(res, test.expected);
    });
  });
});


connection_err = app.__get__('connection_err');
describe('connection_err()', function() {
   it('checks for connection ' , function(done) {
      this.timeout(15000);
      	var expected=[null,false];
    	connection_err('ENOTFOUND', function(err,halted){
     		var result=[err,halted];
     		//console.log(result);
    		assert.deepEqual(result, expected);
    		done();
    	});
      
    });
  });

keep_trying = app.__get__('keep_trying');
describe('keep_trying()', function() {
  
    it('keep trying connection ' , function(done) {
      this.timeout(150000);
      var expected=[null,false];
     keep_trying('ENOTFOUND', 10,function(err,halted,list){
     	var result=[err,halted];
      assert.deepEqual(result, expected);
      done();
      });
      
    });
  });

connection_errors = app.__get__('connection_errors');
describe('connection_errors()', function() {
  
    it('checks for connection ' , function(done) {
      this.timeout(15000);
      var expected=[null,'connection error'];
     connection_errors('ENOTFOUND','a',[],function(err,list){
     	var result=[err,list[0].err];
     	//console.log(result);
     	assert.deepEqual(result, expected);
      done();
      });
      
    });
  });


retry_url = app.__get__('retry_url'); 


describe('retry_url()', function() {
  
    it('retry_url ' , function(done) {
      var url='http://www.google.com';
      this.timeout(150000000);
      var expected=[true,1];
      retry_url(url,2,function(bool,retry){
    
      var result=[bool,retry];
      //console.log(result);
      assert.deepEqual(result, expected);
      done();
      });
      
    });
     it('retry_url ' , function(done) {
      var url='http://www.google.com';
      this.timeout(150000000);
      var expected=[false,0];
      retry_url(url,1,function(bool,retry){
    
      var result=[bool,retry];
      //console.log(result);
      assert.deepEqual(result, expected);
      done();
      });
      
    });
  });


retry_insert = app.__get__('retry_insert'); 
/*describe('retry_insert()', function() {
  
    it('retry_insert ' , function(done) {
      this.timeout(150000);
      var info={Code:'fhffff',val:'dalm'};
      var expected=[false,[]];
      retry_insert(info,2,[],function(retry,changes){
      var result=[retry,changes];
      //console.log(result);
      assert.deepEqual(result, expected);
      done();
      });
      
    });
    it('retry_insert ' , function(done) {
      this.timeout(150000);
      var info={Code:'f',val:'dalm'};
      var expected=[true,[info]];
      retry_insert(info,2,[],function(retry,changes){
      var result=[retry,changes];
      //console.log(result);
      assert.deepEqual(result, expected);
      done();
      });
      
    });
  });
*/
insert_db = app.__get__('insert_db'); 
/*describe('insert_db()', function() {
  
    it('insert_db' , function(done) {
      this.timeout(150000);
      var info={Code:'non',val:'dalm'};
      var expected=[null,info,null,[info]];
      insert_db(info,[],function(err,info,db_err,changes){
      var result=[err,info,db_err,changes];
      //console.log(result);
      assert.deepEqual(result, expected);
      done();
      });
      
    });
  });
*/
check_second_page = app.__get__('check_second_page'); 
describe('check_second_page()', function() {
  
    it('check_second_page ' , function(done) {
      this.timeout(150000000);
      var url='http://www.anam.ma/regulation/guide-medicaments/recherche-de-medicaments-par-nom/?tags=Aranesp&search=type1';
      var expected=[[],'No data retrieved from http://www.anam.ma/regulation/guide-medicaments/recherche-de-medicaments-par-nom/page/2/?tags=Aranesp&search=type1',[]];
     check_second_page(url,[],[],[],function(elems,error_list,changes){
      
      var result=[elems,error_list[0].err,changes];
      //console.log(result);
      assert.deepEqual(result, expected);
      done();
      });
      
    });
   /* it('check_second_page ' , function(done) {
      this.timeout(150000000);
      var url='http://www.anam.ma/regulation/guide-medicaments/recherche-de-medicaments-par-nom/?tags=DOLIPRAN&search=type1';
      var expected=[true];
     check_second_page(url,[],[],[],function(elems,error_list,changes){
    
      var result=[elems,error_list,changes];
      //console.log(result);
      assert.deepEqual(result, expected);
      done();
      });
      
   	 });*/
  });

check_exist = app.__get__('check_exist'); 
describe('check_exist()', function() {
  
    it('checks existence of doc ' , function(done) {
      this.timeout(150000);
    var info={'Code': '6118000140511',
     'Medicament': 'XENID' ,
     'Substance active (DCI) & dosage': 'DICLOFENAC / 50 MG' ,
     'Forme & Presentation':
      'COMPRIME ENROBE GASTRO-RESISTANT1 BOITE 30 COMPRIME GASTRO-RESISTANT' ,
     'Prix Public de Vente (*PPV)': '46,60' ,
     'Prix base remboursement (PPV)': '46,60' ,
     'Prix Hospitalier (**PH )': '29,10' ,
     'Prix base remboursement (PH)': '29,10' ,
     'Classe Therapeutique': 'ANTI-INFLAMMATOIRE NON STEROIDIEN' ,
     'P:Princeps G:Generique': 'O' ,
     'Remboursement': 'Remboursable'  };
      var expected=[null,info,[],[]];
      check_exist(info,[],[],function(err,info,error_list,changes){
      var result=[err,info,error_list,changes];
      //console.log(result);
      assert.deepEqual(result, expected);
      done();
      });
      
    });
  });

find_elem = app.__get__('find_elem'); 
/*describe('find_elem()', function() {
  
    it('find existing elem ' , function(done) {
      this.timeout(150000);
      var elem={'Code': '6118000140511',
     'Medicament': 'XENID' ,
     'Substance active (DCI) & dosage': 'DICLOFENAC / 50 MG' ,
     'Forme & Presentation':
      'COMPRIME ENROBE GASTRO-RESISTANT1 BOITE 30 COMPRIME GASTRO-RESISTANT' ,
     'Prix Public de Vente (*PPV)': '46,60' ,
     'Prix base remboursement (PPV)': '46,60' ,
     'Prix Hospitalier (**PH )': '29,10' ,
     'Prix base remboursement (PH)': '29,10' ,
     'Classe Therapeutique': 'ANTI-INFLAMMATOIRE NON STEROIDIEN' ,
     'P:Princeps G:Generique': 'G' ,
     'Remboursement': 'Remboursable'  };
      var new_data=[{'Code': '61180001',
     'Medicament': 'XENID' ,
     'Substance active (DCI) & dosage': 'DICLOFENAC / 50 MG' ,
     'Forme & Presentation':
      'COMPRIME ENROBE GASTRO-RESISTANT1 BOITE 30 COMPRIME GASTRO-RESISTANT' ,
     'Prix Public de Vente (*PPV)': '46,60' ,
     'Prix base remboursement (PPV)': '46,60' ,
     'Prix Hospitalier (**PH )': '29,10' ,
     'Prix base remboursement (PH)': '29,10' ,
     'Classe Therapeutique': 'ANTI-INFLAMMATOIRE NON STEROIDIEN' ,
     'P:Princeps G:Generique': 'G' ,
     'Remboursement': 'Remboursable'  },{'Code': '6118000140511',
     'Medicament': 'XENID' ,
     'Substance active (DCI) & dosage': 'DICLOFENAC / 50 MG' ,
     'Forme & Presentation':
      'COMPRIME ENROBE GASTRO-RESISTANT1 BOITE 30 COMPRIME GASTRO-RESISTANT' ,
     'Prix Public de Vente (*PPV)': '46,60' ,
     'Prix base remboursement (PPV)': '46,60' ,
     'Prix Hospitalier (**PH )': '29,10' ,
     'Prix base remboursement (PH)': '29,10' ,
     'Classe Therapeutique': 'ANTI-INFLAMMATOIRE NON STEROIDIEN' ,
     'P:Princeps G:Generique': 'G' ,
     'Remboursement': 'Remboursable'  }];
      var expected=[1];
      find_elem(elem,new_data,function(flag){
        //console.log('ggg');
      var result=[flag];
      //console.log(result);
      assert.deepEqual(result, expected);
      done();
      });
      
    });
  });
*/



update2 = app.__get__('update2');
/*describe('update2()', function() {
  
    it('updates2 elem ' , function(done) {
      this.timeout(150000);
      var elem={'_id':'611800014011',
      '_rev':"50-1a00691834025b5b0983be7ed19f7916",
      'Code': '6118000140511',
     'Medicament': 'XENID' ,
     'Substance active (DCI) & dosage': 'DICLOFENAC / 50 MG' ,
     'Forme & Presentation':
      'COMPRIME ENROBE GASTRO-RESISTANT1 BOITE 30 COMPRIME GASTRO-RESISTANT' ,
     'Prix Public de Vente (*PPV)': '46,60' ,
     'Prix base remboursement (PPV)': '46,60' ,
     'Prix Hospitalier (**PH )': '29,10' ,
     'Prix base remboursement (PH)': '29,10' ,
     'Classe Therapeutique': 'ANTI-INFLAMMATOIRE NON STEROIDIEN' ,
     'P:Princeps G:Generique': 'G' ,
     'Remboursement': 'Remboursable'  };
      var new_data={'Code': '6118000140511',
     'Medicament': 'XENID' ,
     'Substance active (DCI) & dosage': 'DICLOFENAC / 50 MG' ,
     'Forme & Presentation':
      'COMPRIME ENROBE GASTRO-RESISTANT1 BOITE 30 COMPRIME GASTRO-RESISTANT' ,
     'Prix Public de Vente (*PPV)': '46,60' ,
     'Prix base remboursement (PPV)': '46,60' ,
     'Prix Hospitalier (**PH )': '29,10' ,
     'Prix base remboursement (PH)': '29,10' ,
     'Classe Therapeutique': 'ANTI-INFLAMMATOIRE NON STEROIDIEN' ,
     'P:Princeps G:Generique': 'G' ,
     'Remboursement': 'sable'  };
      var expected=[null,[]];
      update2(elem,new_data,[],[],function(err,error_list){
      var result=[err,error_list];
      //console.log(result);
      assert.deepEqual(result, expected);
      done();
      });
      
    });
  });
*/

find_non_remboursable_elems = app.__get__('find_non_remboursable_elems');
/*describe('find_non_remboursable_elems()', function() {
  
    it('find_non_remboursable_elems ' , function(done) {
      this.timeout(150000);
      var elem={'_id':'6118000140511',
      '_rev':"51-72ff8397f963035bfa593f21b21f55b9",
      'Code': '6118000140511',
     'Medicament': 'XEND' ,
     'Substance active (DCI) & dosage': 'DICLOFENAC / 50 MG' ,
     'Forme & Presentation':
      'COMPRIME ENROBE GASTRO-RESISTANT1 BOITE 30 COMPRIME GASTRO-RESISTANT' ,
     'Prix Public de Vente (*PPV)': '46,60' ,
     'Prix base remboursement (PPV)': '46,60' ,
     'Prix Hospitalier (**PH )': '29,10' ,
     'Prix base remboursement (PH)': '29,10' ,
     'Classe Therapeutique': 'ANTI-INFLAMMATOIRE NON STEROIDIEN' ,
     'P:Princeps G:Generique': 'G' ,
     'Remboursement': 'Remboursable'  };
      var new_data=[{'Code': '611000140511',
     'Medicament': 'XNID' ,
     'Substance active (DCI) & dosage': 'DICLOFENAC / 50 MG' ,
     'Forme & Presentation':
      'COMPRIME ENROBE GASTRO-RESISTANT1 BOITE 30 COMPRIME GASTRO-RESISTANT' ,
     'Prix Public de Vente (*PPV)': '46,60' ,
     'Prix base remboursement (PPV)': '46,60' ,
     'Prix Hospitalier (**PH )': '29,10' ,
     'Prix base remboursement (PH)': '29,10' ,
     'Classe Therapeutique': 'ANTI-INFLAMMATOIRE NON STEROIDIEN' ,
     'P:Princeps G:Generique': 'G' ,
     'Remboursement': 'Remboursable'  }];
      var expected=[true,[]];
      find_non_remboursable_elems(elem,new_data,[],2,[],function(updated,error_list){
      var result=[updated,error_list];
      //console.log(result);
      assert.deepEqual(result, expected);
      done();
      });
      
    });
  });*/



get_docs = app.__get__('get_docs');
/*describe('get_docs()', function() {
  
    it('retrieve doc ' , function(done) {
      this.timeout(150000);
      var info=[{'Code': '6118000140511',
     'Medicament': 'X' ,
     'Substance active (DCI) & dosage': 'DICLOFENAC / 50 MG' ,
     'Forme & Presentation':
      'COMPRIME ENROBE GASTRO-RESISTANT1 BOITE 30 COMPRIME GASTRO-RESISTANT' ,
     'Prix Public de Vente (*PPV)': '46,60' ,
     'Prix base remboursement (PPV)': '46,60' ,
     'Prix Hospitalier (**PH )': '29,10' ,
     'Prix base remboursement (PH)': '29,10' ,
     'Classe Therapeutique': 'ANTI-INFLAMMATOIRE NON STEROIDIEN' ,
     'P:Princeps G:Generique': 'G' ,
     'Remboursement': 'Remboursable'  },{'Code': '61140511',
     'Medicament': 'XD' ,
     'Substance active (DCI) & dosage': 'DICLOFENAC / 50 MG' ,
     'Forme & Presentation':
      'COMPRIME ENROBE GASTRO-RESISTANT1 BOITE 30 COMPRIME GASTRO-RESISTANT' ,
     'Prix Public de Vente (*PPV)': '46,60' ,
     'Prix base remboursement (PPV)': '46,60' ,
     'Prix Hospitalier (**PH )': '29,10' ,
     'Prix base remboursement (PH)': '29,10' ,
     'Classe Therapeutique': 'ANTI-INFLAMMATOIRE NON STEROIDIEN' ,
     'P:Princeps G:Generique': 'G' ,
     'Remboursement': 'Remboursable'  },{'Code': '610140511',
     'Medicament': 'NID' ,
     'Substance active (DCI) & dosage': 'DICLOFENAC / 50 MG' ,
     'Forme & Presentation':
      'COMPRIME ENROBE GASTRO-RESISTANT1 BOITE 30 COMPRIME GASTRO-RESISTANT' ,
     'Prix Public de Vente (*PPV)': '46,60' ,
     'Prix base remboursement (PPV)': '46,60' ,
     'Prix Hospitalier (**PH )': '29,10' ,
     'Prix base remboursement (PH)': '29,10' ,
     'Classe Therapeutique': 'ANTI-INFLAMMATOIRE NON STEROIDIEN' ,
     'P:Princeps G:Generique': 'G' ,
     'Remboursement': 'Remboursable'  }];
      var expected=[null,[]];
      get_docs(info,[],2,[],function(err,error_list){
      var result=[err,error_list];
      //console.log(result);
      assert.deepEqual(result, expected);
      done();
      });
      
    });
  });
*/
main = app.__get__('main');
/*describe('main()', function() {
  
    it('main ' , function(done) {
      this.timeout(150000000);
      var expected=[];
     main([],1,[],[],function(q,error_list,changes){
    
      var result=[q,error_list,changes];
      //console.log(result);
      assert.deepEqual(result, expected);
      done();
      });
      
    });
  });
*/
set_template = app.__get__('set_template');
/*describe('set_template()', function() {
  
    it('set_template ' , function(done) {
      this.timeout(150000000);
      var expected=[];
     set_template({date:'no',errors:[{var:1,date:'h'},{date:1}]},{date:'hhh',changes:[{var:1,date:'h'},{date:1}]},function(f){
    
      var result=[f];
      //console.log(result);
      assert.deepEqual(result, expected);
      done();
      });
      
    });
  });
*/
send_email = app.__get__('send_email');
describe('send_email()', function() {
  
    it('send_email ' , function(done) {
      this.timeout(150000000);
      var expected=['Email sent'];
     send_email({date:'no',errors:[{var:1,date:'h'},{date:1}]},{date:'hhh',changes:[{var:1,date:'h'},{date:1}]},function(f){
    
      var result=[f];
     // //console.log(result);
      assert.deepEqual(result, expected);
      done();
      });
      
    });
  });

submain = app.__get__('submain');
describe('submain()', function() {
  
    it('submain ' , function(done) {
      this.timeout(150000000);
      var expected=[[], 
          "This URL: http://www.anam.ma/wp-admin/admin-ajax.php?action=get_Medicament&term=y has no content." ,[]];
     submain('y',[],0,[],[],function(queue,error_list,changes){
      var result=[queue,error_list[0].err,changes];
    	 //console.log(error_list[0]);
      assert.deepEqual(result, expected);
      done();
      });
      
    });
  });

