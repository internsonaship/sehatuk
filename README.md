# SehatukUpdate API Documentation#
### Description:###

SehatukUpdate is designed to update the database of the medicines list in the already existing app "Sehatuk". It provides a a well ordered and cosistent database of medicines that can be linked to Sehatuk.

### Technologies:###

SehatukUpdate was made with Node.js, Couchdb as a non-relational, NoSQL Database, and NewRelic as a realtime follow up of the environment state. 

### Installation:###

#####Please make sure to perform the following installations before starting the program:#####

- First step is to download Node.js from their website ([following this link](https://nodejs.org/en/download/)). It's recommended to use the latest LTS Version: v6.11.1 that includes npm 3.10.10, which is also required.
- Second most inmportant step is to download Couchdb, which can be easily done using their website ([following this link](http://couchdb.apache.org/)). You can easily check if it's well installed by first running on your machine 'Start CouchDB'. Then, go on http://localhost:5984/_utils/. If there are no errors then it's well installed.  
- After that, please proceed to installing the following using command line:

        - $ npm install request;
        - $ npm install nano;
        - $ npm install cheerio;
        - $ npm install nodemailer;
        - $ npm install bottleneck;
        - $ npm install newrelic;
        - $ npm install pug;
        - $ npm install admin-npm;

    ####Please make sure that before running the app, in order for the database (CouchDb) to work, it needs to be started by launching 'Start CouchDB'.####

### Core of the App:###

The application does the following:

#### Getting the names of medicines:####

1. Starts by sending requests to the ANAM website (www.anam.ma). The requests performed are GET requests using urls that fetch the list of names of medicines based on alphabet.
2. Get the content of each URL.
    The medicines are in a JSON format each one having a name and a value {name:.... , value:.....}. The names can usually be repeated if a medicine has a different dose or form. Hence, to avoid duplicates, the value part is used instead of the name.

#### Getting the information of each medicine:####

1. Starts by sending requests to the ANAM website (www.anam.ma). The requests performed are GET requests by using the values fetched earlier in the previous step and including them in the urls. This returns the html of the page. 
    The html of the page has a component list-medicament, which is the list/table that has the information of the medicine searched and requested by the url.
2. Using the Cheerio module, the html is analyzed and the list-medicament table is extracted and formatted as an array/list.

#### Parsing the information:####

1. Since the search/request may result in several medicines in one table, the Cheerio module by default makes each medicine as a column. So instead we transpose the list and make the medicines as separate rows. 
##### For each row:#####

2. The information goes under format errors processing in order to check for different/erronous format.
3. If no issue happens the information is parsed into a JSON object.

#### Checking the existence of the medicine:####

1. Once the medicine is parsed, its existence in the database is checked. If it exists then we check for any updates that may have happened. If so we update our database. If it doesn't exist then we insert it in the database.


#### Checking the non-refundable medicines:####

1. Since the list on the website only contains refundable medicines, that list is compared everytime with the existing database in order to spot mecines that no longer belong to the website data list.
2. After those medicines are found they are updated in the database and turned into non-refundable medicines.

#### Saving changes and errors into database ####

After keeping track of most of the errors, and all the changes, each of the two lists is converted into a JSON object:

1. changes: {changes:[list of changes, each change is in itself a JSON obj {change:...., time:....}] , time:....}
2. errors: {errors:[list of errors, each error is in itself a JSON obj {error:...., time:....}] , time:....}

#### Sending emails to admins ####

1. After the entire update is made and saving the changes and errors, a  template is prepared. This template contains a count of the errors and the changes as well as links to the appropriate database documents.
2. Next is the set up of the email to be sent. This is done using the Nodemailer module from npm. 
3. The email is sent to the administrators.

